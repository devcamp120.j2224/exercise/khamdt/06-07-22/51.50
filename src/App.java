public class App {
    public static void main(String[] args) throws Exception {
        int myNum = 5;               // integer (whole number)
    float myFloatNum = 5.99f;    // floating point number
    char myLetter = 'D';         // character
    boolean myBool = true;       // boolean
    String myText = "Hello";     // String
    double myDuble = 20;        // double
    byte myByte = 0;            // byte
    short myShort = 6;
    int myInt = 7;
    long myLong = 232;
     
    System.out.println(myNum);
    System.out.println(myFloatNum);
    System.out.println(myLetter);
    System.out.println(myBool);
    System.out.println(myText);
    System.out.println(myDuble);
    System.out.println(myByte);
    System.out.println(myShort);
    System.out.println(myInt);
    System.out.println(myLong);
    

    }
}
